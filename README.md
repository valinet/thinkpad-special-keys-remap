I introduce you my new application: ThinkPad Special Key Remap. This application allows you to remap the 4 special keys at the far right of the ThinkPad keyboard on models like the W540 (XX4X) or so. Basically, newer models, prior to the current P50, and P70, as far as I know.

The 4 keys I am talking about are the Calculator, Lock, Browser, and Folder keys. You can map this to any program on your computer, or even volume down/up, and mute, for example, like they are on current generation ThinkPads (i.e. the new P50 and P70).

Currently, this is the initial release. Here is the supported features list: * Remap all the four mentioned keys; by default, Calculator is volume down, Lock is mute, Internet is volume up, and Folder opens calc.exe - the reasoning for this can be deducted by reading the technical specs bellow, plus the fact that I frequently use the Calculator key. * Enable / Disable the app by pressing Windows + N, with audio confirmation (or not) * Run in the background, do not eat a lot of CPU/RAM, has icon in system tray * Ability to launch at startup, elevated, as the app needs to be elevated in order to run (i.e. run as admin)

Current limitations/issues/bugs: * When launched by Task Scheduler at startup, sometimes, the icon in the notification tray is not spawned - still investigating, but the application works

Requirements * Windows Vista or more (I would also say XP, who knows...) - I developed it on Windows 10 build 1511 * Microsoft .NET Framework 4 Complete * Obviously, it works best on ThinkPads

Download (includes actual app, plus source code) Application is in key_preview\key preview\key preview\bin\Debug folder, it is called "key preview.exe" https://googledrive.com/host/0BzZ1AE59CpFgVVhLZ2RCeWZ2VE0/key_preview.zip

Enjoy

Technical information * This application is open-source, the full source is available to have a look at. The keyboard hook is based on an example from CodeProject, plus some other class I have since a long time ago, and I don't remember where I have got it from, yet I am sure it was free to use as well (check About in app for more info) * The app is written in C#, using Visual Studio 2015 Update 1 * Majority of code is commented, here is the most important part of commentary, which tells how the four keys actually work/behave from a technical point of view:

Summary: the way the hack works is by looking at the time which has elapsed between key presses, and acting accordingly

The way the 4 ThinkPad special keys at the far right of the keyboard work is the following: 1. Calculator key - this is simply a VK_LAUNCH_APP2 key, or better said, sends the following signal when pressed; when held down, it sends it multiple times (really important, you'll see why in a bit) 2. Lock key - this is more complicated: it sends the combination Windows + L, as if it were send by the user; when held down, it sends nothing (I mean, it sends Windows+L once, and that's it) 3. Internet key - this again simply sends VK_BROWSER_HOME; when held down, it continously sends that, practicly spawning multiple browser windows, when left on default 4. Folder key - again, this one sends Windows + E, and behaves similarly to the lock key

Now, the hack works differently for the 4 keys: 1. For Calculator, and Internet, the app listens for each time the key is pressed, and instead of performing the system default, performs whatever it is set in the main window to do. Easy. Because when held down, the keys keep sending the signal of being pressed, the app maps them by default to volume down/up, so that when you keep your finger pressed on it, the volume continously decreases/increases. Clever. 2. For Lock and Folder, it is way more complicated: basically, the app checks the time difference between when the Windows key was pressed, and the other key in the combination was pressed (the E, or the L). This is because the ThinkPad keys send the signals incredibly fast one after the other, which is impossible to do in the physical world (i.e. with your fingers). So, when the time difference is incredibly low, our custom actions are performed. When it is big enough o seem physical, the default actions for Windows+E, and Windows + L are performed (i.e. spawn Explorer, and lock the workstation). If you were to be able to hit for example Windows + L in under 50ms (the time difference I set for decision between key presses), then the volume would be muted, instead of locking the workstation indeed. But I think it is impossible to be that fast anyway, so I think the current implementation works good enough.

An alternative to what I did here may be to take the keyboard apart, and cut and solder the corresponding wires accordingly - it might be risky, but someone may find it worth trying...

Because I have to listen to combinations of keys, I had to disable the keys altogether, wait for both of them to be pressed, and then decide what to do based on the time difference. This means I had to replicate spawning explorer, and locking workstation on my own

You saw at the begining of the code how I disable locking the workstation by Windows + L via Windows' own method. That is because, even though my app catches Windows + L and tells Windows that it is handled, Windows still does not believe it and proceeds to locking the workstation anyway on its own (it is obvious why it does this : security, so no one can tamper with critical key combos - the same applies for CTRL+ALT+DEL for example). So, I ahd to disable the functionality altogether, and Windows fortunately offers a registry key to do so, and then replicate it whenever Windows+L is pressed via the combination of keys, and not using the Lock button

One more thing: I told you that the Lock and Folder keys are sent once, and then that's it - check yourself: you cannot spawn multiple explorer windows when the folder key is held down. This means, unfortunately, that you cannot map repetitive actions to this, as they won't work: if you map volume up/down, the volume would increase /decrease once, and then stay there, even though the key is still pressed.

Thanks for checking it out, enjoy!
