﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace key_preview
{
    public static class WinHotKeys
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool RegisterHotKey(IntPtr hWnd, int id, int fsModifiers, int vk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        private const int HOTKEY_ID = 0x0000;
        private const int MOD_WIN = 0x0008;

        public static void Register(IntPtr hWnd, int keycode)
        {
            try
            {
                if (!RegisterHotKey(hWnd, HOTKEY_ID, MOD_WIN, keycode))
                    throw new Exception();
            }
            catch
            {
//                Environment.Exit(0);
            }
        }

        public static void Unregister(IntPtr hWnd)
        {
            if (!UnregisterHotKey(hWnd, HOTKEY_ID))
                throw new Exception();
        }
    }
}
