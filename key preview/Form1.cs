using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Utilities;
using Microsoft.Win32;
using System.Diagnostics;
using WindowsInput;

namespace key_preview {
	public partial class Form1 : Form {
		globalKeyboardHook gkh = new globalKeyboardHook();

        private const int APPCOMMAND_VOLUME_MUTE = 0x80000;
        private const int APPCOMMAND_VOLUME_UP = 0xA0000;
        private const int APPCOMMAND_VOLUME_DOWN = 0x90000;
        private const int WM_APPCOMMAND = 0x319;

        [DllImport("user32.dll")]
        public static extern IntPtr SendMessageW(IntPtr hWnd, int Msg,
            IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool LockWorkStation();

        public Form1() {
			InitializeComponent();
		}
        const int WM_HOTKEY = 0x0312;
        protected override void WndProc(ref Message m)
        {
            // we use this to detect whether Windows + N was pressed, and toggle the app status between enabled/disabled if true
            // you may be wondering why not detect Windows+L and Windows+E the same way, well because of 2 reasons:
            // 1. I needed to know the time difference between the key presses of the keys in the combination
            // 2. Which is the main reason it does not work this way, is because in order to subscribe like this to keys, it is 
            //    necessary for it to be free (i.e. no one else already subscribed to it) - sure, both combinations can be 
            //    disabled by registry, but still we couldn't work around #1
            switch (m.Msg)
            {

                case WM_HOTKEY:
                    if (m.LParam.ToString() == "5111816")
                    {
                        checkBox1.Checked = !checkBox1.Checked;
                        if (checkBox2.Checked) System.Media.SystemSounds.Beep.Play(); //optional, of course
                    }
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        private void Form1_Load(object sender, EventArgs e) {
            SystemEvents.PowerModeChanged += OnPowerChange;

            // register Windows + N as toggle for the application enabled/disabled status
            WinHotKeys.Register(this.Handle, 0x4E);

            // load actions from registry, into the main window
            calcAction.Text = Properties.Settings.Default.calculatorAction;
            lockAction.Text = Properties.Settings.Default.lockAction;
            internetAction.Text = Properties.Settings.Default.internetAction;
            folderAction.Text = Properties.Settings.Default.folderAction;
            checkBox2.Checked = Properties.Settings.Default.beep;
            checkBox3.Checked = Properties.Settings.Default.eggbean;
            numericUpDown1.Value = Properties.Settings.Default.delay;
            checkBox4.Checked = Properties.Settings.Default.terminal;
            path.Text = Properties.Settings.Default.cmdpath;

            // disable workstation lock from the OS level, in order to correctly handle the lock key; Windows + L is not affected
            // this is one of the reasons the application must be run elevated, the other being that by having elevation,
            // the application can modify the behavour of the keys in elevated applications too (if the app wouldn't have been
            // elevated, it couldn't alter the behavour of keys in other apps which were elevated)
            SetLockW(1);

            // register hooks for the LWin key, E, L, BrowserHome, which is the Internet key, and LaunchApplication2, which 
            // is the Calculator key
            gkh.HookedKeys.Add(Keys.LWin);
			gkh.HookedKeys.Add(Keys.E);
            gkh.HookedKeys.Add(Keys.L);
            gkh.HookedKeys.Add(Keys.BrowserHome);
            gkh.HookedKeys.Add(Keys.LaunchApplication2);

            gkh.HookedKeys.Add(Keys.PageDown);
            gkh.HookedKeys.Add(Keys.PageUp);
            gkh.HookedKeys.Add(Keys.LControlKey);
            gkh.HookedKeys.Add(Keys.LMenu);
            gkh.HookedKeys.Add(Keys.T);

            gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
			gkh.KeyUp += new KeyEventHandler(gkh_KeyUp);
		}

		void gkh_KeyUp(object sender, KeyEventArgs e) {
            if (enabled)
            {
                if (checkBox3.Checked)
                {
                    if (e.KeyCode == Keys.LControlKey) prev = "";
                }
            }
            //clears the list no matter what
            lstLog.Items.Clear();
		}
        string prev, old_prev;
		void gkh_KeyDown(object sender, KeyEventArgs e) {
            if (enabled) // performs the trickery only when the app is enabled
            {
                if (checkBox3.Checked)
                {
                    if (e.KeyCode == Keys.PageDown && prev == "LControlKey")
                    {
                        e.Handled = true;
                        enabled = false;
                        InputSimulator.SimulateKeyUp(VirtualKeyCode.LCONTROL);
                        SendKeys.Send("{PGDN}");
                        enabled = true;
                        return;
                    }
                    else if (e.KeyCode == Keys.PageUp && prev == "LControlKey")
                    {
                        e.Handled = true;
                        enabled = false;
                        InputSimulator.SimulateKeyUp(VirtualKeyCode.LCONTROL);
                        SendKeys.Send("{PGUP}");
                        enabled = true;
                        return;
                    }
                    else if (e.KeyCode == Keys.PageDown)
                    {
                        e.Handled = true;
                        enabled = false;
                        InputSimulator.SimulateKeyDown(VirtualKeyCode.BROWSER_FORWARD);
                        enabled = true;
                    }
                    else if (e.KeyCode == Keys.PageUp)
                    {
                        e.Handled = true;
                        enabled = false;
                        InputSimulator.SimulateKeyDown(VirtualKeyCode.BROWSER_BACK);
                        enabled = true;
                    }
                }
                if (e.KeyCode == Keys.BrowserHome) // the internet, aka globe icon was pressed, so we perform the custom action
                {
                    e.Handled = true; // this ensures that the OS does not handle this also itself, i.e. we take care of it
                    if (internetAction.Text == "volume up") // this is a shortcut, when on default, no external app is spawned
                        SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_UP);
                    else
                        Process.Start(internetAction.Text.Split(' ')[0], internetAction.Text.Split(' ')[1]);
                }
                else if (e.KeyCode == Keys.LaunchApplication2)
                {
                    e.Handled = true; // this ensures that the OS does not handle this also itself, i.e. we take care of it
                    if (calcAction.Text == "volume down") // this is a shortcut, when on default, no external app is spawned
                        SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_DOWN);
                    else
                        Process.Start(calcAction.Text.Split(' ')[0], calcAction.Text.Split(' ')[1]);
                }
                else if (checkBox4.Checked && e.KeyCode == Keys.T && prev == "LMenu" && old_prev == "LControlKey")
                {
                    e.Handled = true;
                    Process.Start(path.Text);
                }
                // Summary: the way the hack works is by looking at the time which has elapsed between key presses, and acting 
                // accordingly
                //
                // The way the 4 ThinkPad special keys at the far right of the keyboard work is the following:
                // 1. Calculator key - this is simply a VK_LAUNCH_APP2 key, or better said, sends the following signal when 
                //    pressed; when held down, it sends it multiple times (really important, you'll see why in a bit)
                // 2. Lock key - this is more complicated: it sends the combination Windows + L, as if it were send by the user; 
                //    when held down, it sends nothing (I mean, it sends Windows+L once, and that's it)
                // 3. Internet key - this again simply sends VK_BROWSER_HOME; when held down, it continously sends that, practicly
                //    spawning multiple browser windows, when left on default
                // 4. Folder key - again, this one sends Windows + E, and behaves similarly to the lock key
                //
                // Now, the hack works differently for the 4 keys:
                // 1. For Calculator, and Internet, the app listens for each time the key is pressed, and instead of performing 
                //    the system default, performs whatever it is set in the main window to do. Easy. Because when held down, the 
                //    keys keep sending the signal of being pressed, the app maps them by default to volume down/up, so that when 
                //    you keep your finger pressed on it, the volume continously decreases/increases. Clever.
                // 2. For Lock and Folder, it is way more complicated: basically, the app checks the time difference between when 
                //    the Windows key was pressed, and the other key in the combination was pressed (the E, or the L). This is 
                //    because the ThinkPad keys send the signals incredibly fast one after the other, which is impossible to do in
                //    the physical world (i.e. with your fingers). So, when the time difference is incredibly low, our custom 
                //    actions are performed. When it is big enough o seem physical, the default actions for Windows+E, and Windows
                //    + L are performed (i.e. spawn Explorer, and lock the workstation). If you were to be able to hit for example
                //    Windows + L in under 50ms (the time difference I set for decision between key presses), then the volume 
                //    would be muted, instead of locking the workstation indeed. But I think it is impossible to be that fast 
                //    anyway, so I think the current implementation works good enough.
                //
                // An alternative to what I did here may be to take the keyboard apart, and cut and solder the corresponding wires
                // accordingly - it might be risky, but someone may find it worth trying...
                //
                // Because I have to listen to combinations of keys, I had to disable the keys altogether, wait for both of them 
                // to be pressed, and then decide what to do based on the time difference. This means I had to replicate spawning 
                // explorer, and locking workstation on my own
                //
                // You saw at the begining of the code how I disable locking the workstation by Windows + L via Windows' own 
                // method. That is because, even though my app catches Windows + L and tells Windows that it is handled, Windows 
                // still does not believe it and proceeds to locking the workstation anyway on its own (it is obvious why it does 
                // this : security, so no one can tamper with critical key combos - the same applies for CTRL+ALT+DEL for 
                // example). So, I ahd to disable the functionality altogether, and Windows fortunately offers a registry key to 
                // do so, and then replicate it whenever Windows+L is pressed via the combination of keys, and not using the Lock 
                // button
                //
                // One more thing: I told you that the Lock and Folder keys are sent once, and then that's it - check yourself: 
                // you cannot spawn multiple explorer windows when the folder key is held down. This means, unfortunately, that 
                // you cannot map repetitive actions to this, as they won't work: if you map volume up/down, the volume would 
                // increase /decrease once, and then stay there, even though the key is still pressed.
                DateTime now = DateTime.Now;
                lstLog.Items.Add(now.Second.ToString());
                lstLog.Items.Add(now.Millisecond.ToString());
                if (lstLog.Items.Count == 4)
                {
                    int ii = Convert.ToInt32(lstLog.Items[0]); // initial second
                    int iu = Convert.ToInt32(lstLog.Items[1]); // initial milisecond
                    int fi = Convert.ToInt32(lstLog.Items[2]); // final second
                    int fu = Convert.ToInt32(lstLog.Items[3]); // final milisecond
                    if (ii == fi && (fu - iu) <= numericUpDown1.Value && (prev == "LWin" && (e.KeyCode == Keys.E || e.KeyCode == Keys.L)))
                    {
                        e.Handled = true; // this ensures that the OS does not handle this also itself, i.e. we take care of it
                        if (e.KeyCode == Keys.E) System.Diagnostics.Process.Start(folderAction.Text.Split(' ')[0], folderAction.Text.Split(' ')[1]); 
                        else if (e.KeyCode == Keys.L)
                        {
                            if (lockAction.Text == "volume mute") // this is a shortcut, when on default, no external app is spawned
                                SendMessageW(this.Handle, WM_APPCOMMAND, this.Handle, (IntPtr)APPCOMMAND_VOLUME_MUTE);
                            else
                                Process.Start(lockAction.Text.Split(' ')[0], lockAction.Text.Split(' ')[1]);
                        }
                    }
                    else if (prev == "LWin" && e.KeyCode == Keys.L) // detects whether the actual physical combination Windows + L was pressed
                    {
                        enabled = false; // disables the app
                        SetLockW(0); // enables locking the workstation natively with Windows + L
                        LockWorkStation(); // locks the workstation
                        LockWorkStation(); // locks the workstation
                        SetLockW(1); // sets back previous conditions
                        enabled = true; // all of this is necessary because the LockWorkStation() WINAPI does not work if the physical locking by Windows + L is disabled via the registry key; we disable the application in order not to enter a loop
                    }
                    lstLog.Items.Clear();
                }
                old_prev = prev;
                prev = e.KeyCode.ToString();
                if (prev == "LWin" && (e.KeyCode == Keys.E || e.KeyCode == Keys.L)) //makes sure the OS understands we handled it
                    e.Handled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Hide();
            this.Opacity = 1;
            timer1.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("ThinkPad Special Keys Remap" + Environment.NewLine + Application.ProductVersion + Environment.NewLine + Environment.NewLine + "Coding by Valentin-Gabriel Radu, vali20 @ MDL" + Environment.NewLine + "Freeware, based on keyboard hooking code available at http://www.codeproject.com/Articles/19004/A-Simple-C-Global-Low-Level-Keyboard-Hook, and InputSimulator from http://inputsimulator.codeplex.com/." + Environment.NewLine + Environment.NewLine + "Thanks for using!", "ThinkPad Special Keys Remap", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        bool enabled = true;
        private void disableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            checkBox1.Checked = !checkBox1.Checked;        } //weirdly enough, this is the only way it worked
        private void EnableDisable() // enables / disables the application
        {
            if (enabled)
            {
                disableToolStripMenuItem.Text = "Enable";
                checkBox1.Checked = false;
                enabled = false;
                gkh.KeyDown -= new KeyEventHandler(gkh_KeyDown);
                gkh.KeyUp -= new KeyEventHandler(gkh_KeyUp);
                SetLockW(0);

            }
            else
            {
                disableToolStripMenuItem.Text = "Disable";
                checkBox1.Checked = true;
                enabled = true;
                gkh.KeyDown += new KeyEventHandler(gkh_KeyDown);
                gkh.KeyUp += new KeyEventHandler(gkh_KeyUp);
                SetLockW(1);

            }
            lstLog.Items.Clear();
        }
        private void configureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        protected override void OnSizeChanged(EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized) this.Hide(); // hides the app when minimized
            base.OnSizeChanged(e);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            EnableDisable();
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;

        }
        private void SetLockW(int what)
        { //   this registry key sets whether Windows + L is handled by Windows and lock the workstation
            // elevation is required, ad the registry key cannot be accessed by standard users, even though it is in the HKCU context
            RegistryKey registrybrowser = Registry.CurrentUser.OpenSubKey
       (@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);

            if (registrybrowser == null)
            {
                RegistryKey registryFolder = Registry.CurrentUser.OpenSubKey
                    (@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies", true);
                registrybrowser = registryFolder.CreateSubKey("System");
            }
            registrybrowser.SetValue("DisableLockWorkstation", what, RegistryValueKind.DWord);
            registrybrowser.Close();
        }
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            SetLockW(0); // reverts back to default setting
            //saves actions
            Properties.Settings.Default.calculatorAction = calcAction.Text;
            Properties.Settings.Default.lockAction = lockAction.Text;
            Properties.Settings.Default.internetAction = internetAction.Text;
            Properties.Settings.Default.folderAction = folderAction.Text;
            Properties.Settings.Default.beep = checkBox2.Checked;
            Properties.Settings.Default.delay = Convert.ToInt32(numericUpDown1.Value);
            Properties.Settings.Default.eggbean = checkBox3.Checked;
            Properties.Settings.Default.terminal = checkBox4.Checked;
            Properties.Settings.Default.cmdpath = path.Text;
            Properties.Settings.Default.Save();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            button2.PerformClick();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value = 20;
            Properties.Settings.Default.delay = Convert.ToInt32(numericUpDown1.Value);
            Properties.Settings.Default.Save();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            // Creates an XML file from which a scheduled task is created and saved on the system. Based on an example found on StackOverflow, http://stackoverflow.com/questions/5427673/how-to-run-a-program-automatically-as-admin-on-windows-startup
            string username = Environment.UserName;
            string computername = Environment.MachineName;
            string part1 = new System.IO.StreamReader(Application.StartupPath + "\\part1.txt").ReadToEnd();
            string part2 = new System.IO.StreamReader(Application.StartupPath + "\\part2.txt").ReadToEnd();
            string part3 = new System.IO.StreamReader(Application.StartupPath + "\\part3.txt").ReadToEnd();
            string part4 = new System.IO.StreamReader(Application.StartupPath + "\\part4.txt").ReadToEnd();
            string final = part1 + username + part2 + computername + "\\" + username + part3 + Application.ExecutablePath + part4;
            System.IO.StreamWriter sw = new System.IO.StreamWriter(Application.StartupPath + "\\apply.xml");
            sw.Write(final);
            sw.Close();
            Process pr = new Process();
            ProcessStartInfo pi = new ProcessStartInfo();
            pi.FileName = "cmd.exe";
            pi.Arguments = "/c schtasks /create /tn \"Start ThinkPad Special Key Remap elevated\" /xml \"" + Application.StartupPath + "\\apply.xml\"";
            pr.StartInfo = pi;
            pr.Start();
            pr.WaitForExit();
            DialogResult dr = MessageBox.Show("Registration complete, do you want to open task scheduler to check the operation, or further adjust the entry which was created?", "ThinkPad Special Key Remap", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.Yes) Process.Start("Taskschd.msc");
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                path.Text = openFileDialog1.FileName;
            }
        }

        void OnPowerChange(Object sender, PowerModeChangedEventArgs e)
        {
            switch (e.Mode)
            {
                case PowerModes.Resume:
                    enabled = true;
                    SetLockW(1);
                    break;
                case PowerModes.Suspend:
                    enabled = false;
                    SetLockW(0);
                    /*for (int i = 0; i < 10; i++)
                       LockWorkStation();
                    SetLockW(1);
                    enabled = true;*/
                    break;
            }
        }
    }
}