using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace key_preview {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern bool SetProcessDPIAware();
        [STAThread]
        static void Main() {
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(Application_ThreadException);
            Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            if (Environment.OSVersion.Version.Major >= 6 && Environment.OSVersion.Platform == PlatformID.Win32NT) SetProcessDPIAware();
            Application.Run(new Form1());
		}
        static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            Environment.Exit(0);
        }
        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            System.Diagnostics.Process.Start(Application.ExecutablePath);
            Environment.Exit(0);

        }

    }
}